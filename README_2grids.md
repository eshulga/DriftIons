# Drift of Electrons and Ions through 2 grids

Electrons and ions are drifted separately.

$`E_{d}=320`$ V/cm

## 

- **for $'Ar/CH_{4}'$ (90:10):**

![electrons](./run/Plots/new_MC_T_ArCH4_90_10_EtEd.png)  | ![electrons Zoom](./run/Plots/new_MC_T_ArCH4_90_10.png) 
:-------------------------:|:-------------------------:
$`E_{t}=320`$ V/cm| $`E_{t}=160`$ V/cm

- **for $'Ne/CF_{4}'$ (90:10):**

![electrons](./run/Plots/new_MC_T_NeCF4_90_10_EtEd.png)  | ![electrons Zoom](./run/Plots/new_MC_T_NeCF4_90_10.png) 
:-------------------------:|:-------------------------:
$`E_{t}=320`$ V/cm| $`E_{t}=160`$ V/cm
