#include <iostream>

#include <TCanvas.h>
#include <TROOT.h>
#include <TApplication.h>

#include "Garfield/MediumMagboltz.hh"
#include "Garfield/FundamentalConstants.hh"

using namespace Garfield;

int main(int argc, char * argv[]) {

  TApplication app("app", &argc, argv);
 
  const double pressure = 1 * AtmosphericPressure;
  const double temperature = 293.15;
 
  // Setup the gas.
  MediumMagboltz gas;
  gas.SetComposition("Ne", 90., "CF4", 10.);
  gas.SetTemperature(temperature);
  gas.SetPressure(pressure);

  // Set the field range to be covered by the gas table. 
  const int nFields = 20;
  const double emin = 100.;
  const double emax = 100000.;
  // Set the field range to be covered by the gas table. 
  const int nBFields = 4;
  const double bmin = 0.0;
  const double bmax = 1.2;
  // Flag to request logarithmic spacing.
  constexpr bool useLog = true;
  gas.SetFieldGrid(emin, emax, nFields, useLog, bmin, bmax, nBFields,0, HalfPi, 2); 

  const int ncoll = 10;
  // Run Magboltz to generate the gas table.
  gas.GenerateGasTable(ncoll);
  // Save the table. 
  gas.WriteGasFile("./Ne_90_CF4_10_new.gas");

  // app.Run(kTRUE);

}
