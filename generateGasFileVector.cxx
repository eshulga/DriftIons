#include <iostream>

#include <TCanvas.h>
#include <TROOT.h>
#include <TApplication.h>

#include "Garfield/MediumMagboltz.hh"
#include "Garfield/FundamentalConstants.hh"

using namespace Garfield;

int main(int argc, char * argv[]) {

  TApplication app("app", &argc, argv);
 
  const double pressure = 1 * AtmosphericPressure;
  const double temperature = 293.15;
 
  // Setup the gas.
  MediumMagboltz gas;
  //gas.SetComposition("Ne", 90., "CF4", 10.);
  gas.SetComposition("Ar", 90., "CH4", 10.);
  gas.SetTemperature(temperature);
  gas.SetPressure(pressure);

  // Set the field range to be covered by the gas table. 
  //std::vector<double> EFields = {0., 100., 200., 300., 400., 500. , 600., 320., 480., 4276.7, 5132.};//1283 on anode
  std::vector<double> EFields = { 0., 100., 200.,  320., 400., 480., 600., 800., 1000., 2000., 4276.7, 5132., 10000, 100000, 1000000};//1283 on anode
  std::vector<double> BFields = {0., 1.2};
  //std::vector<double> ASet    = {0., HalfPi,  HalfPi/2, HalfPi/4, HalfPi/8,  HalfPi/16,  HalfPi/32};
  std::vector<double> ASet    = {0., HalfPi/512, HalfPi/256, HalfPi/128,  HalfPi/32, HalfPi/16, HalfPi/8, HalfPi/4, HalfPi/3, HalfPi/2, HalfPi, 1.3*HalfPi, 1.6*HalfPi, 2*HalfPi};
  
  gas.SetFieldGrid(EFields,BFields,ASet); 

  const int ncoll = 10;
  // Run Magboltz to generate the gas table.
  gas.GenerateGasTable(ncoll);
  // Save the table. 
  //gas.WriteGasFile("./Ne_90_CF4_10_vector_v1.gas");
  gas.WriteGasFile("./Ar_90_CH4_10_vector_v1.gas");

  // app.Run(kTRUE);

}
