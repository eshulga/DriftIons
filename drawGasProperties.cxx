#include <iostream>
#include <cstdlib>

#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>

#include "Garfield/MediumMagboltz.hh"
#include "Garfield/ViewMedium.hh"
#include "Garfield/FundamentalConstants.hh"

using namespace Garfield;

int main(int argc, char * argv[]) {

  TApplication app("app", &argc, argv);
 
  // Setup the gas.
  MediumMagboltz gas;
  gas.LoadGasFile("./Ne_90_CF4_10_vector_v1.gas");
  // gas.PrintGas();
  std::vector<double> efields;
  std::vector<double> bfields;
  std::vector<double> angles;
  gas.GetFieldGrid(efields, bfields, angles);

  ViewMedium view;
  view.SetMedium(&gas);
  view.SetMagneticField(1.2);
 
  // Plot the velocity as function of electric field 
  // at the first non-zero angle in the table. 
  TCanvas c1("c1", "", 800, 600);
  view.SetCanvas(&c1);
  if (!angles.empty()) view.SetAngle(angles[5]);
  // Set the x-axis limits explicitly.
  view.EnableAutoRangeX(false);
  // Plot only the low-field part, using linear scale.
  view.SetRangeE(100.,100001., false);
  view.PlotElectronVelocity('e');
  view.PlotHoleVelocity('e', true);
  // Plot the velocity as function of angle between E and B,
  // at E = 400 V / cm.
  TCanvas c2("c2", "", 800, 600);
  view.SetCanvas(&c2);
  view.SetElectricField(5132.);
  view.EnableAutoRangeX(false);
  view.SetRangeA(0.0001, HalfPi+0.01, false);
  view.PlotElectronVelocity('a');

  app.Run(kTRUE);

}
