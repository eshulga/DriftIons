# Drift of Electrons and Ions through the detector

## Overview

Detector is simulated to represent full setup:
- Drift electrode;
- Bi-polar grid (1 mm pitch);
- Transfer mesh: 0.5 mm by 0.5 mm wiresquares are represented with wires of 0.3 mm pitch to emulate 90% optical transparancy;
- Amplification and field wires with 2.5 mm pitch

The distances between electrodes from bottom to the top:
- 3 mm;
- 3 mm;
- 4 mm;
- 12.5 mm

Electrons and ions are drifted separately.

- **for B=0, $`\Delta V=60`$ V**:

![electrons](./run/Plots/electrons_B0_dV60_1000.png)  | ![electrons Zoom](./run/Plots/electrons_B0_dV60.png) 
:-------------------------:|:-------------------------:
Electrons| Electrons Zoom

![ions](./run/Plots/ions_B0_dV60_1000.png)  | ![ions Zoom](./run/Plots/ions_B0_dV60.png) 
:-------------------------:|:-------------------------:
ions| ions Zoom

- **for B=1.2, $`\Delta V=60`$ V**:

![electrons](./run/Plots/electrons_B1p2_dV60_1000.png)  | ![electrons Zoom](./run/Plots/electrons_B1p2_dV60.png) 
:-------------------------:|:-------------------------:
Electrons| Electrons Zoom

![ions](./run/Plots/ions_B1p2_dV60_1000.png)  | ![ions Zoom](./run/Plots/ions_B1p2_dV60.png) 
:-------------------------:|:-------------------------:
ions| ions Zoom

It is performed in the [DriftIonsThroughGrid.cxx](https://git.racf.bnl.gov/gitea/shulga/DriftIons/src/branch/master/DriftIonsThroughGrid.cxx)

## Gas file generation 

Generated for Ne/CF$`_{4}`$ 90:10

It is performed in the [generateGasFileVector.cxx](https://git.racf.bnl.gov/gitea/shulga/DriftIons/src/branch/master/generateGasFileVector.cxx)

The values of the electric fields and angles are:

| E [V/cm] | $`\theta`$ [rad]|
|:---:|:---:|
|0|0|
|100|$`\pi`$/256|
|200|$`\pi`$/64|
|320|$`\pi`$/32|
|400|$`\pi`$/16|
|480|$`\pi`$/8|
|600|$`\pi`$/6|
|800|$`\pi`$/4|
|1000|$`\pi`$/2|
|2000|
|4276.7|
|5132|
|10'000|
|100'000|

B = 0 and 1.2 T

Results of the gas file simulation for drift velocities @ B=1.2 T:

![electrons](./run/Plots/EB.png)  | ![ions](./run/Plots/E.png) 
:-------------------------:|:-------------------------:
E=5132 V/cm| $`\theta`$=$`\pi`$/8


## Results

Transparancy is calculated as number of charges absorbed on the grid divided by the number of charges floating in the electrode.

![T MC](./run/Plots/mc_to_data.png)  |
:-------------------------:|

Open markers represent ion transparancy, filled markers are electron transparancy

