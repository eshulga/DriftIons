// #ifndef __CINT__

#include <iostream>
#include <fstream>

#include <TApplication.h>
#include "TFile.h"
#include "TString.h"
#include "TH1.h"
#include "TH2.h"
#include "TProfile.h"
#include "TNtuple.h"
#include "TRandom.h"


#include "Garfield/ViewField.hh"
#include "Garfield/ViewCell.hh"
#include "Garfield/ComponentAnalyticField.hh"
#include "Garfield/MediumMagboltz.hh"
#include "Garfield/Sensor.hh"
#include "Garfield/ViewDrift.hh"
#include "Garfield/FundamentalConstants.hh"
#include "Garfield/DriftLineRKF.hh"
#include "Garfield/ViewMedium.hh"
#include "Garfield/ViewSignal.hh"
#include "Garfield/Random.hh"

#include <string.h>
#include <random>

using namespace std;
using namespace Garfield;
//char gasFileName[]="./Ne_90_CF4_10_vector_v1.gas";
char gasFileName[]="./Ar_90_CH4_10_vector_v1.gas";

int DriftIonsThroughGrid(double deltav = 0., double Bz = 0., int gain = 100, bool batch = false, string SaveDir="./Files/");
int DriftElectronsThroughGrid(double deltav = 0., double Bz = 0., int gain = 100, bool batch = false, string SaveDir="./Files/");

// Impulse response of the PASA
double transfer(double t) {
  const double tau = 160;
  const double fC_to_mV = 12.7;
  return fC_to_mV * exp(4) * pow((t / tau), 4) * exp(-4 * t / tau);  
}

int main(int argc, char * argv[]) {
  double dv=-1;
  double Bz=0;
  int gain=0;
  int batch=0;
  bool fBatch=false;
  string savedir;
  if (argc>1) {
    dv=atof(argv[1]);
    Bz=atof(argv[2]);
    gain=atof(argv[3]);
    batch=atof(argv[4]);
    savedir=argv[5];
    if (batch==1)fBatch=true;  
    printf("dV=%f\n",dv);
    printf("B_z=%f\n",Bz);
    printf("G=%d\n",gain);
  }
  TApplication app("app", &argc, argv);
  if (argc>1){
    DriftIonsThroughGrid(dv,Bz, gain, fBatch, savedir);
    DriftElectronsThroughGrid(dv,Bz, gain, fBatch, savedir);
  }else{
    printf("NO INPUT -> DEFAULT SETUP IS USED!!!\n");
    DriftIonsThroughGrid();
    DriftElectronsThroughGrid();
  }
  if(!fBatch){
    app.Run(kTRUE);
  }
}

int DriftIonsThroughGrid(double deltav,double Bz, int gain, bool batch, string SaveDir ){
  
  // Switch between IROC and OROC.
  const bool iroc = false;

  // Switch gating on or off.
  const bool gating = true;

  // Voltage settings [V]
  const double vSens =  1283.;
  const double vGate = -192;
  //const double deltav = 0.;//40.; // for closed gate mode
 
  // y-axis gap between rows of wires
  const double gap =  0.3;
 
  // y coordinates of the wires
  const double ys = gap;            // anode wires
  const double yc = 2. * gap;       // cathode
  const double yg = 2. * gap + 0.4; // gate
 
  // y coordinate and voltage of the HV plane (drift field)
  const double yHV = 2.3;
  const double vHV = -592;
 
  // Periodicity (wire spacing) [cm]
  const double period = 0.25;
  const int nRep = 10;
 
  const double dc = 0.03;//period /5;
  const double dg = period / 2.5;
 
  // Wire diameters [cm]
  const double dSens = 0.0050;//0.0050;
  const double dCath = 0.0075;//0.0050;
  const double dGate = 0.0050;//0.0050;
 
  // Setup the gas.
  MediumMagboltz* gas = new MediumMagboltz();
  // Set the temperature [K] and pressure [Torr].
  const double pressure = 1 * AtmosphericPressure;; 
  const double temperature = 293.15;
  gas->SetTemperature(temperature);
  gas->SetPressure(pressure);
  // Set the composition.
  //gas->SetComposition("Ne", 90, "CF4", 10.);
  gas->SetComposition("Ar", 90, "CH4", 10.);

  // Read the electron transport coefficients from a .gas file.
  //gas->LoadGasFile("../Ne_90_CO2_10_N2_5_with_mg.gas");
  gas->LoadGasFile(gasFileName);
  // Read the ion mobility table.
  const std::string garfpath = std::getenv("GARFIELD_HOME");
  //gas->LoadIonMobility(garfpath + "/Data/IonMobility_Ne+_Ne.txt");
  gas->LoadIonMobility(garfpath + "/Data/IonMobility_Ar+_Ar.txt");

  // Setup the electric field.
  ComponentAnalyticField* comp = new ComponentAnalyticField();
  comp->SetMedium(gas);
  //comp->SetPeriodicityX(nRep * period);
  // Add the sense wires.
  for (int i = 0; i < nRep + 10; ++i) {
    const double xs = (i - 10.) * period;
    if ( i % 2 == 0)
      comp->AddWire(xs, ys, dSens, vSens, "s+", 3., 150., 19.3, 1);
    else
      comp->AddWire(xs, ys, dSens, 0., "s-", 3., 150., 19.3, 1);
  }
  // Add the cathode wires.
  for (int i = 0; i < nRep *16; ++i) {
    const double xc = dc * (i -80);
    comp->AddWire(xc, yc, dCath, 0., "c", 3., 50., 7.9, 1);
  }
  // Add the gate wires.
  if (gating) {
    for (int i = 0; i < 50; i += 1) {
      const double xg = dg * (i - 25);
      //printf("%d Xg+=%f\n",i,xg);
      if ( i % 2 == 0)
        comp->AddWire(xg, yg, dGate, vGate + deltav, "g+", 3., 150., 19.3, 1);
      else      
        comp->AddWire(xg, yg, dGate, vGate - deltav, "g-", 3., 150., 19.3, 1);
    }

  } else {
    for (int i = 0; i < nRep * 7; ++i) {
      const double xg = dg * (i + 0.5);
      // Set trap radius to 1 to avoid stopping the drift line 
      // prematurely at the gating grid.
      comp->AddWire(xg, yg, dGate, vGate, "g", 3., 150., 19.3, 1);  
    }
  }
  // Add the planes.
  comp->AddPlaneY(0., 0., "pad_plane");
  comp->AddPlaneY(yHV-0.05, vHV, "HV");

  // Request weighting-field calculation for the pad plane. 
  comp->AddReadout("pad_plane");
 
  // Set the magnetic field [T].
  comp->SetMagneticField(0, Bz, 0);

  // Make a sensor (gating closed).
  Sensor* sensor = new Sensor();
  sensor->AddComponent(comp);
  //sensor->AddElectrode(comp, "pad_plane");
  // Change the time window for less/better resolution in time 
  // (effect on convolution can be important).
  //sensor->SetTimeWindow(0.,1,50000); 

  // Plot isopotential contours
  ViewField* fView = 0;
  if(!batch){
    fView = new ViewField;
    fView->SetSensor(sensor);
    fView->SetArea(-nRep * period , 0.,
                    nRep * period , 2);
    fView->SetVoltageRange(-600., 1500.);
    fView->PlotContour();
  }
  // Plot the cell layout.
  TCanvas*  cCell = 0;
  ViewCell* cellView = 0;
  if(!batch){
    cCell = new TCanvas("cCell_i","cCell_i",600,800);
    cellView = new ViewCell();
    cellView->SetCanvas(cCell);
    cellView->SetArea(-3 * period / 2., -0.1, -600., 
                       3 * period, 2.3, +600.);
    cellView->SetComponent(comp); 
    cellView->Plot2d();
  }

  // Calculate ion drift lines using the RKF method.
  DriftLineRKF* driftline = new DriftLineRKF();
  //DriftLineRKF* driftline_e = new DriftLineRKF();
  // driftline->EnableDebugging();
  // driftline->EnableVerbose();
  driftline->SetSensor(sensor);
  //driftline_e->SetSensor(sensor);
        
  // Plot the drift line.
  // Comment this out when calculating many drift lines.
  ViewDrift* driftView = 0;
  if(!batch){
    driftView = new ViewDrift();
    driftView->SetCanvas(cCell);
    driftline->EnablePlotting(driftView);
  }

  TNtuple *ntuple = new TNtuple("ntuple","Demo ntuple","p:f:c:g:e:Gain:Tig:Tim");
  // const int gain = 10000;
  //const int gain = 100;
  double r = 0.003;
  double dummy = 0.;
  int status = 0;
  double endpoint = gap;
  // Count the number of ions that drift to 
  // plane, cathode, gate or drift volume, respectively.
  int plane = 0, field = 0 , cathode = 0, gate = 0, escape = 0; 
  double Tig = 0, Tim = 0;
  for (int i = 0; i < gain; i++) {
    double angle = RndmGaussian(0, 1.4);
    driftline->DriftIon(r * sin(angle), gap + r * cos(angle), 0, 0);
    //driftline_e->DriftElectron(r * sin(angle), gap + r * cos(angle), 0, 0);
    driftline->GetEndPoint(dummy,endpoint,dummy,dummy,status);
    if (endpoint < gap / 2) {
      // Drifted to plane.
      ++plane;
    } else if (endpoint > 0.5 * gap && endpoint < 1.5 * gap) {
      // Drifted to cathode wires.
      ++field;
    } else if (endpoint > 1.5 * gap && endpoint < 2.5 * gap) {
      // Drifted to cathode wires.
      ++cathode;
    } else if (endpoint > 2.5 * gap && endpoint <2 * gap + 0.4 * 1.5) {
      // Drifted to gating wires.
      ++gate;
    } else {
      // Escaped to the drift volume.
      ++escape;
    }
  }
  Tig=double(escape)/(escape+gate);
  Tim=double(escape+gate)/(escape+gate+cathode);
  ntuple->Fill(plane,field,cathode,gate,escape,gain,Tig, Tim);
  // Plot the drift lines on top of the cell layout.
  if(!batch)driftView->Plot(true, false); 

  // Plot the induced current.
  ViewSignal* vs1 = 0;
  if(!batch){
    //vs1 = new ViewSignal();
    //vs1->SetSensor(sensor);
    //vs1->PlotSignal("pad_plane");
  }
  // Convolute with the transfer function and plot.
  ViewSignal* vs2 = 0;
  if(!batch){
    //vs2 = new ViewSignal();
    //sensor->SetTransferFunction(transfer);
    // sensor->ConvoluteSignal();
    //vs2->SetSensor(sensor);
    //vs2->PlotSignal("pad_plane");
  }

  char str[100];
  //float f = 123.456789;
  snprintf(str, sizeof(str), "File_dV_%.0f_Bz_%.1f_i.root", deltav,Bz);

  TString FileName=SaveDir+str;

  TFile hfile(FileName,"RECREATE","ROOT file with ntuple");

  ntuple->Write();
  
  if(!batch)cCell ->Write();

  // Save all objects in this file
  hfile.Write();

  // Close the file. Note that this is automatically done when you leave
  // the application.
  hfile.Close();

 
  return 0;
}

int DriftElectronsThroughGrid(double deltav,double Bz, int gain, bool batch, string SaveDir ){
  
  // Switch between IROC and OROC.
  const bool iroc = false;

  // Switch gating on or off.
  const bool gating = true;

  // Voltage settings [V]
  const double vSens =  1283.;
  const double vGate = -192;
  //const double deltav = 0.;//40.; // for closed gate mode
 
  // y-axis gap between rows of wires
  const double gap =  0.3;
 
  // y coordinates of the wires
  const double ys = gap;            // anode wires
  const double yc = 2. * gap;       // cathode
  const double yg = 2. * gap + 0.4; // gate
 
  // y coordinate and voltage of the HV plane (drift field)
  const double yHV = 2.3;
  const double vHV = -592;
 
  // Periodicity (wire spacing) [cm]
  const double period = 0.25;
  const int nRep = 10;
 
  const double dc = 0.03;//period /5;
  const double dg = period / 2.5;
 
  // Wire diameters [cm]
  const double dSens = 0.0050;
  const double dCath = 0.0075;
  const double dGate = 0.0050;
 
  // Setup the gas.
  MediumMagboltz* gas = new MediumMagboltz();
  // Set the temperature [K] and pressure [Torr].
  const double pressure = 1 * AtmosphericPressure; 
  const double temperature = 293.15;
  gas->SetTemperature(temperature);
  gas->SetPressure(pressure);
  // Set the composition.
  //gas->SetComposition("ne", 85.72, "co2", 9.52, "n2", 4.76);
  //gas->SetComposition("Ne", 90., "CF4", 10.);
  gas->SetComposition("Ar", 90., "CH4", 10.);

  // Read the electron transport coefficients from a .gas file.
  //gas->LoadGasFile("../Ne_90_CO2_10_N2_5_with_mg.gas");
  gas->LoadGasFile(gasFileName);
  // Read the ion mobility table.
  const std::string garfpath = std::getenv("GARFIELD_HOME");
  //gas->LoadIonMobility(garfpath + "/Data/IonMobility_Ne+_Ne.txt");
  gas->LoadIonMobility(garfpath + "/Data/IonMobility_Ar+_Ar.txt");

  // Setup the electric field.
  ComponentAnalyticField* comp = new ComponentAnalyticField();
  comp->SetMedium(gas);
  //comp->SetPeriodicityX(nRep * period);
  // Add the sense wires.
  for (int i = 0; i < nRep + 10; ++i) {
    const double xs = (i - 10.) * period;
    if ( i % 2 == 0)
      comp->AddWire(xs, ys, dSens, vSens, "s+", 3., 150., 19.3, 1);
    else
      comp->AddWire(xs, ys, dSens, 0., "s-", 3., 150., 19.3, 1);
  }
  // Add the cathode wires.
  for (int i = 0; i < nRep *16; ++i) {
    const double xc = dc * (i -80);
    comp->AddWire(xc, yc, dCath, 0., "c", 3., 50., 7.9, 1);
  }
  // Add the gate wires.
  if (gating) {
    for (int i = 0; i < 50; i += 1) {
      const double xg = dg * (i - 25);
      //printf("%d Xg+=%f\n",i,xg);
      if ( i % 2 == 0)
        comp->AddWire(xg, yg, dGate, vGate + deltav, "g+", 3., 150., 19.3, 1);
      else      
        comp->AddWire(xg, yg, dGate, vGate - deltav, "g-", 3., 150., 19.3, 1);
    }

  } else {
    for (int i = 0; i < nRep * 7; ++i) {
      const double xg = dg * (i + 0.5);
      // Set trap radius to 1 to avoid stopping the drift line 
      // prematurely at the gating grid.
      comp->AddWire(xg, yg, dGate, vGate, "g", 3., 150., 19.3, 1);  
    }
  }
  // Add the planes.
  comp->AddPlaneY(0., 0., "pad_plane");
  comp->AddPlaneY(yHV-0.05, vHV, "HV");

  // Request weighting-field calculation for the pad plane. 
  comp->AddReadout("pad_plane");
 
  // Set the magnetic field [T].
  comp->SetMagneticField(0, Bz, 0);

  // Make a sensor (gating closed).
  Sensor* sensor = new Sensor();
  sensor->AddComponent(comp);
  //sensor->AddElectrode(comp, "pad_plane");
  // Change the time window for less/better resolution in time 
  // (effect on convolution can be important).
  //sensor->SetTimeWindow(0.,1,50000); 

  // Plot isopotential contours
  ViewField* fView = 0;
  if(!batch){
    fView = new ViewField;
    fView->SetSensor(sensor);
    fView->SetArea(-nRep * period , 0.,
                    nRep * period , 2);
    fView->SetVoltageRange(-600., 1500.);
    fView->PlotContour();
  }
  // Plot the cell layout.
  TCanvas*  cCell = 0;
  ViewCell* cellView = 0;
  if(!batch){
    cCell = new TCanvas("cCell_e","cCell_e",600,800);
    cellView = new ViewCell();
    cellView->SetCanvas(cCell);
    cellView->SetArea(-3 * period / 2., -0.1, -600., 
                       3 * period, 2.3, +600.);
    cellView->SetComponent(comp); 
    cellView->Plot2d();
  }

  // Calculate ion drift lines using the RKF method.
  DriftLineRKF* driftline = new DriftLineRKF();
  // driftline->EnableDebugging();
  // driftline->EnableVerbose();
  driftline->SetSensor(sensor);   
  
  // Calculate ion drift lines using the RKF method.
  //DriftLineRKF* driftline_i = new DriftLineRKF();
  //driftline_i->SetSensor(sensor);
  // Plot the drift line.
  // Comment this out when calculating many drift lines.
  ViewDrift* driftView = 0;
  if(!batch){
    driftView = new ViewDrift();
    driftView->SetCanvas(cCell);
    driftline->EnablePlotting(driftView);
  }

  TNtuple *ntuple = new TNtuple("ntuple","Demo ntuple","p:f:c:g:e:Gain:Teg:Tem");
  // const int gain = 10000;
  //const int gain = 100;
  double r = 0.25;
  double dummy = 0.;
  int status = 0;
  double endpoint = gap;
  // Count the number of ions that drift to 
  // plane, cathode, gate or drift volume, respectively.
  int plane = 0, field = 0 , cathode = 0, gate = 0, escape = 0; 
  double Teg = 0, Tem = 0;
  for (int i = 0; i < gain; i++) {
    // double angle_x = RndmUniformPos();
    // double angle_y = RndmUniform();
    // driftline->DriftElectron(r * (2*angle_x-1), 2*gap+0.4+gap*(2.05-2*angle_y), 0, 0);
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);//uniform distribution between 0 and 1
    driftline->DriftElectron(-period + 2 * RndmUniform() * period,
                             yg + 0.3*dis(gen)+0.05, 0, 0);  
    driftline->GetEndPoint(dummy,endpoint,dummy,dummy,status);
    //driftline_i->DriftIon(-period + 2 * RndmUniform() * period,
    //                         yg - 0.2, 0, 0);

    if (endpoint < gap / 2) {
      // Drifted to plane.
      ++plane;
    } else if (endpoint > 0.5 * gap && endpoint < 1.5 * gap) {
      // Drifted to cathode wires.
      ++field;
    } else if (endpoint > 1.5 * gap && endpoint < 2.5 * gap) {
      // Drifted to cathode wires.
      ++cathode;
    } else if (endpoint > 2.5 * gap && endpoint <2 * gap + 0.4 * 1.5) {
      // Drifted to gating wires.
      ++gate;
    } else {
      // Escaped to the drift volume.
      ++escape;
    }
  }
  Teg=double(plane+field+cathode)/(plane+field+cathode+gate);
  Tem=double(plane+field)/(plane+field+cathode);
  ntuple->Fill(plane,field,cathode,gate,escape,gain,Teg, Tem);
  // Plot the drift lines on top of the cell layout.
  if(!batch)driftView->Plot(true, false); 

  // Plot the induced current.
  ViewSignal* vs1 = 0;
  if(!batch){
    //vs1 = new ViewSignal();
    //vs1->SetSensor(sensor);
    //vs1->PlotSignal("pad_plane");
  }
  // Convolute with the transfer function and plot.
  ViewSignal* vs2 = 0;
  if(!batch){
    //vs2 = new ViewSignal();
    //sensor->SetTransferFunction(transfer);
    // sensor->ConvoluteSignal();
    //vs2->SetSensor(sensor);
    //vs2->PlotSignal("pad_plane");
  }

  char str[100];
  //float f = 123.456789;
  snprintf(str, sizeof(str), "File_dV_%.0f_Bz_%.1f_e.root", deltav,Bz);

  TString FileName=SaveDir+str;

  TFile hfile(FileName,"RECREATE","ROOT file with ntuple");

  ntuple->Write();
  
  if(!batch)cCell ->Write();

  // Save all objects in this file
  hfile.Write();

  // Close the file. Note that this is automatically done when you leave
  // the application.
  hfile.Close();


  return 0;
}
