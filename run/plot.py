import sys
sys.path.append("/Users/eshulga/Work/sPHENIX/Grid/cau4np-master/datafmts")
import math as mt
import numpy as np
import matplotlib.pyplot as plt
import struct
from collections import namedtuple
from ROOT import TFile, TTree, TH1F, TCanvas, TPad, TF1, TGraph, TLegend, TLegendEntry
from array import array
import re

files_B1p2_e=[
    './Files/File_dV_60_Bz_1.2_e.root',
    './Files/File_dV_50_Bz_1.2_e.root',
    './Files/File_dV_40_Bz_1.2_e.root',
    './Files/File_dV_30_Bz_1.2_e.root',
    './Files/File_dV_25_Bz_1.2_e.root',
    './Files/File_dV_20_Bz_1.2_e.root',
    './Files/File_dV_15_Bz_1.2_e.root',
    './Files/File_dV_10_Bz_1.2_e.root',
    './Files/File_dV_5_Bz_1.2_e.root',
    './Files/File_dV_0_Bz_1.2_e.root'
]
files_B1p2_i=[
    './Files/File_dV_60_Bz_1.2_i.root',
    './Files/File_dV_50_Bz_1.2_i.root',
    './Files/File_dV_40_Bz_1.2_i.root',
    './Files/File_dV_30_Bz_1.2_i.root',
    './Files/File_dV_25_Bz_1.2_i.root',
    './Files/File_dV_20_Bz_1.2_i.root',
    './Files/File_dV_15_Bz_1.2_i.root',
    './Files/File_dV_10_Bz_1.2_i.root',
    './Files/File_dV_5_Bz_1.2_i.root',
    './Files/File_dV_0_Bz_1.2_i.root'
]

files_B0_e=[
    './Files/File_dV_60_Bz_0.0_e.root',
    './Files/File_dV_50_Bz_0.0_e.root',
    './Files/File_dV_40_Bz_0.0_e.root',
    './Files/File_dV_30_Bz_0.0_e.root',
    './Files/File_dV_25_Bz_0.0_e.root',
    './Files/File_dV_20_Bz_0.0_e.root',
    './Files/File_dV_15_Bz_0.0_e.root',
    './Files/File_dV_10_Bz_0.0_e.root',
    './Files/File_dV_5_Bz_0.0_e.root',
    './Files/File_dV_0_Bz_0.0_e.root'
]

files_B0_i=[
    './Files/File_dV_60_Bz_0.0_i.root',
    './Files/File_dV_50_Bz_0.0_i.root',
    './Files/File_dV_40_Bz_0.0_i.root',
    './Files/File_dV_30_Bz_0.0_i.root',
    './Files/File_dV_25_Bz_0.0_i.root',
    './Files/File_dV_20_Bz_0.0_i.root',
    './Files/File_dV_15_Bz_0.0_i.root',
    './Files/File_dV_10_Bz_0.0_i.root',
    './Files/File_dV_5_Bz_0.0_i.root',
    './Files/File_dV_0_Bz_0.0_i.root'
]

def make_graph(x,fileList,fT="c"):
    y=[]
    for fname in fileList:
        f = TFile.Open(fname)
        nt=f.Get("ntuple")
        nt.GetEntry(0)
        T = getattr(nt, fT)
        y.append(T)
        f.Close()
    
    nP=len(x)
    g=TGraph(nP,array('d',x),array('d',y))
    return g

def make_hist(x,fileList,fT="c",name="h_c"):
    y=[]
    for fname in fileList:
        f = TFile.Open(fname)
        nt=f.Get("ntuple")
        nt.GetEntry(0)
        T = getattr(nt, fT)
        y.append(T)
        f.Close()
    
    nP=len(x)
    h=TH1F(name,name,100,-1,99)
    for i,j in zip(array('d',x),array('d',y)):
        h.Fill(i,j)
        print(i,end=" ")
        print(j)
        
        bin=h.FindBin(i)
        h.SetBinError(bin,j**0.5)
    
    return h

x=[60.,50.,40.,30.,25.,20.,15.,10.,5.,0.]


g_B1p2_e=make_graph(x,files_B1p2_e,"Teg")
g_B0_e  =make_graph(x,files_B0_e,  "Teg")

g_B1p2_i = make_graph(x,files_B1p2_i,"Tig")
g_B0_i   = make_graph(x,files_B0_i  ,"Tig")

h_B0_f_e   = make_hist (x,files_B0_e      ,"f","h_f")
h_B1p2_f_e   = make_hist (x,files_B1p2_e  ,"f","h_f")


g_B1p2_Tim=make_graph(x,files_B1p2_i,"Tim")
g_B0_Tim  =make_graph(x,files_B0_i  ,"Tim")

c=TCanvas("c","c",800,600)
c.SetRightMargin(0.0215)
c.SetLeftMargin(0.13)
c.SetBottomMargin(0.13)
c.SetTopMargin(0.03)

g_B0_e  .SetMarkerColor(614)
g_B0_e  .SetMarkerStyle(34)

g_B0_i  .SetMarkerColor(614)
g_B0_i  .SetMarkerStyle(28)

g_B1p2_i.SetMarkerStyle(24)

h =TH1F("h","h",200000,-200,200)

h  .SetNdivisions(1005, "X")
h  .SetNdivisions(1005, "Y")
h  .SetTickLength(0.025,"X")
h  .SetTickLength(0.025,"Y")
h  .SetTitleSize(0.062,"Y")
h  .SetTitleSize(0.062,"X")
h  .SetTitleOffset(.950,"Y")
h  .SetTitleOffset(.90,"X")
h  .SetLabelSize(0.059,"Y")
h  .SetLabelSize(0.059,"X")
h  .SetLabelOffset(.007,"Y")
h  .SetLabelOffset(.005,"X")

h.GetYaxis().SetRangeUser(-0.045,1.039)
h.GetXaxis().SetRangeUser(0,72)
h.GetYaxis().SetTitle("#it{T}")
h.GetXaxis().SetTitle("#Delta#it{V}")

h.Draw("AXIS")
g_B1p2_e.Draw("Psame")
g_B0_e  .Draw("Psame")
g_B1p2_i.Draw("Psame")
g_B0_i  .Draw("Psame")






leg  =TLegend(0.6,0.35,0.9,0.6,"","brNDC")
leg  .SetFillStyle(0)
leg  .SetTextSize(0.04)
leg  .SetTextAlign(12)
leg  .SetBorderSize(0)
leg  .SetTextFont(42)

leg  .AddEntry(g_B1p2_e,"T^{g}_{e} B=1.2 T","p")
leg  .AddEntry(g_B0_e  ,"T^{g}_{e} B=0","p")
leg  .AddEntry(g_B1p2_i,"T^{g}_{i} B=1.2 T","p")
leg  .AddEntry(g_B0_i  ,"T^{g}_{i} B=0","p")

leg  .Draw()

c.Print("./Plots/c.png")

c_Tim=TCanvas("c_Tim","c_Tim",800,600)

g_B0_Tim  .SetMarkerColor(2)
g_B0_Tim  .SetMarkerStyle(28)

g_B1p2_Tim.SetMarkerStyle(24)

g_B1p2_Tim.Draw("AP")
g_B0_Tim  .Draw("Psame")

g_B1p2_Tim.GetYaxis().SetRangeUser(0.3,0.5)

c_Tim.Print("./Plots/c_Tim.png")

c_Teg_f=TCanvas("c_Teg_f","c_Teg_f",800,600)

max_B0_f_e  =h_B0_f_e  .GetMaximum()
max_B1p2_f_e=h_B1p2_f_e.GetMaximum()

h_B0_f_e  .Scale(0.95/max_B0_f_e  )
h_B1p2_f_e.Scale(0.95/max_B1p2_f_e)

h_B0_f_e  .SetMarkerColor(2)
h_B0_f_e  .SetMarkerStyle(34)

h_B0_f_e.GetXaxis().SetRange(0,65)

h_B0_f_e  .Draw("E1X0")
h_B1p2_f_e.Draw("E1X0same")

h_B0_f_e.GetYaxis().SetTitle("#it{T}")
h_B0_f_e.GetXaxis().SetTitle("#Delta#it{V}")
h_B0_f_e  .Draw("AXISsame")

c_Teg_f.Print("./Plots/Teg_f.png")

