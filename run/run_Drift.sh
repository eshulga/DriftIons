#Before running:
#- cmake ../
#- make
#DriftIonsThroughGrid   dV B Nevt batch FileDir
#B=0
./DriftIonsThroughGrid  0.0 0.0 10000 1 ./Files/
./DriftIonsThroughGrid  5.  0.0 10000 1 ./Files/
./DriftIonsThroughGrid  10. 0.0 10000 1 ./Files/
./DriftIonsThroughGrid  15. 0.0 10000 1 ./Files/
./DriftIonsThroughGrid  20. 0.0 10000 1 ./Files/
./DriftIonsThroughGrid  25. 0.0 10000 1 ./Files/
./DriftIonsThroughGrid  30. 0.0 10000 1 ./Files/
./DriftIonsThroughGrid  40. 0.0 10000 1 ./Files/
./DriftIonsThroughGrid  50. 0.0 10000 1 ./Files/
./DriftIonsThroughGrid  60. 0.0 10000 1 ./Files/
#B=1.2 T
./DriftIonsThroughGrid  0.0 1.2 10000 1 ./Files/
./DriftIonsThroughGrid  5.  1.2 10000 1 ./Files/
./DriftIonsThroughGrid  10. 1.2 10000 1 ./Files/
./DriftIonsThroughGrid  15. 1.2 10000 1 ./Files/
./DriftIonsThroughGrid  20. 1.2 10000 1 ./Files/
./DriftIonsThroughGrid  25. 1.2 10000 1 ./Files/
./DriftIonsThroughGrid  30. 1.2 10000 1 ./Files/
./DriftIonsThroughGrid  40. 1.2 10000 1 ./Files/
./DriftIonsThroughGrid  50. 1.2 10000 1 ./Files/
./DriftIonsThroughGrid  60. 1.2 10000 1 ./Files/

